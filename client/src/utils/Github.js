export default class Github {
  baseURL = new URL('https://api.github.com/')

  constructor(per_page = 20) {
    this.per_page = per_page
  }

  searchRepositories(query, page = 1) {
    const url = this.baseURL
    url.pathname = 'search/repositories'
    url.searchParams.append('q', query)
    url.searchParams.append('page', page)
    url.searchParams.append('per_page', this.per_page)
    console.log(url.href)
    return fetch(url.href);
  }
}