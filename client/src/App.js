import React, { Component } from 'react'
import SearchBar from 'components/SearchBar'
import Title from 'components/Title'
import Details from 'components/Details'
import Github from 'utils/Github'
import 'styles/App.css'


export default class App extends Component {

  handleSearch = (input) => {
    const github = new Github();

    github.searchRepositories(input)
      .then(response => { return response.json() })
      .then(data => { console.log(data) })
  }

  render() {
    return (
      <div className="app-container">
        <Title>Glu</Title>
        <SearchBar onSubmit={this.handleSearch} />
        <Details>Easy to seach across platforms</Details>
      </div>
    )
  }
}
