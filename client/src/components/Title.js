import React, { Component } from 'react'
import 'styles/Title.css'

export default class Title extends Component {
  render() {
    return (
      <h1 className="title-text">{this.props.children}</h1>
    )
  }
}
