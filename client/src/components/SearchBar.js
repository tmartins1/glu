import React, { Component } from 'react';
import 'styles/SearchBar.css'

export default class SearchBar extends Component {
  state = { value: '' };

  onChange = event => {
    this.setState({ value: event.target.value })
  }

  onSubmit = event => {
    event.preventDefault()
    this.props.onSubmit(this.state.value)
  }

  render() {
    return (
      <div className="search-bar-container">
        <form className="search-bar-form" onSubmit={this.onSubmit}>
          <input className="search-bar-input" type="text" value={this.state.value} onChange={this.onChange} />
          <button hidden type="submit">Submit</button>
        </form>
      </div>
    );
  }
}